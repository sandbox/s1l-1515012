<?php
/**
 * @file
 * commerce_dutch_vat.features.inc
 */

/**
 * Implements hook_commerce_tax_default_rates().
 */
function commerce_dutch_vat_commerce_tax_default_rates() {
  $items = array(
    'btw_hoog_19' => array(
      'name' => 'btw_hoog_19',
      'display_title' => 'BTW 19%',
      'description' => '19% BTW',
      'rate' => '.19',
      'type' => 'vat',
      'rules_component' => 'commerce_tax_rate_btw_hoog_19',
      'default_rules_component' => '1',
      'price_component' => 'tax|btw_hoog_19',
      'calculation_callback' => 'commerce_tax_rate_calculate',
      'module' => 'commerce_tax_ui',
      'title' => 'BTW Hoog 19',
      'admin_list' => TRUE,
    ),
    'btw_laag_6' => array(
      'name' => 'btw_laag_6',
      'display_title' => 'BTW 6%',
      'description' => '6% BTW',
      'rate' => '.06',
      'type' => 'vat',
      'rules_component' => 'commerce_tax_rate_btw_laag_6',
      'default_rules_component' => '1',
      'price_component' => 'tax|btw_laag_6',
      'calculation_callback' => 'commerce_tax_rate_calculate',
      'module' => 'commerce_tax_ui',
      'title' => 'BTW Laag 6',
      'admin_list' => TRUE,
    ),
  );
  return $items;
}
