<?php
/**
 * @file
 * commerce_dutch_vat.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function commerce_dutch_vat_default_rules_configuration() {
  $items = array();
  $items['commerce_tax_rate_btw_hoog_19'] = entity_import('rules_config', '{ "commerce_tax_rate_btw_hoog_19" : {
      "LABEL" : "Calculate BTW Hoog 19",
      "PLUGIN" : "rule",
      "REQUIRES" : [ "commerce_tax" ],
      "USES VARIABLES" : { "commerce_line_item" : { "type" : "commerce_line_item", "label" : "Line item" } },
      "DO" : [
        { "commerce_tax_rate_apply" : {
            "USING" : {
              "commerce_line_item" : [ "commerce-line-item" ],
              "tax_rate_name" : "btw_hoog_19"
            },
            "PROVIDE" : { "applied_tax" : { "applied_tax" : "Applied tax" } }
          }
        }
      ]
    }
  }');
  $items['commerce_tax_rate_btw_laag_6'] = entity_import('rules_config', '{ "commerce_tax_rate_btw_laag_6" : {
      "LABEL" : "Calculate BTW Laag 6",
      "PLUGIN" : "rule",
      "REQUIRES" : [ "commerce_tax" ],
      "USES VARIABLES" : { "commerce_line_item" : { "type" : "commerce_line_item", "label" : "Line item" } },
      "DO" : [
        { "commerce_tax_rate_apply" : {
            "USING" : {
              "commerce_line_item" : [ "commerce-line-item" ],
              "tax_rate_name" : "btw_laag_6"
            },
            "PROVIDE" : { "applied_tax" : { "applied_tax" : "Applied tax" } }
          }
        }
      ]
    }
  }');
  return $items;
}
